// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/03/a/PC.hdl

/**
 * A 16-bit counter with load and reset control bits.
 * if      (reset[t] == 1) out[t+1] = 0
 * else if (load[t] == 1)  out[t+1] = in[t]
 * else if (inc[t] == 1)   out[t+1] = out[t] + 1  (integer addition)
 * else                    out[t+1] = out[t]
 */

CHIP PC {
    IN in[16],load,inc,reset;
    OUT out[16];

    PARTS:
    // Increment
    ALU(x=Cout, y=false, zx=false, nx=true, zy=true, ny=true, f=true, no=true, out=incrout, zr=zr, ng=ng);
    Mux16(a=Cout, b=incrout, sel=inc, out=Iout);
    // Load
    Mux16(a=Iout, b=in, sel=load, out=Lout);
    // Reset
    Mux16(a=Lout, b=false, sel=reset, out=Rout);
    Or(a=load, b=inc, out=or1);
    Or(a=or1, b=reset, out=or2);
    Register(in=Rout, load=or2, out=out, out=Cout);
}
