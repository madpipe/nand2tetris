// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed.
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

@R0
M = 0
@R1
M = 0
@R2
M = 0

@SCREEN
D=A
@8192   // 256 * (512 / 16) = 8192
D = D + A
@LastScreenRegister
M = D

(WAIT)
    @KBD
    D = M
    @R2
    M = D
    @SET_FILL_BLACK
    D; JNE
    @R0
    M = 0
(CHECK_TOGGLE)
    @R1
    D = M
    @R2
    D = D - M
    @SET_TOGGLE
    D; JNE
    @WAIT
    0; JMP

(SET_TOGGLE)
    @R2
    D = M
    @R1
    M = D
    @FILL
    0; JMP

(SET_FILL_BLACK)
    @R0
    M = -1
    @CHECK_TOGGLE
    0; JMP

(FILL)
    @SCREEN                 // Init i
    D = A
    @i
    M = D

(LOOP)
    @LastScreenRegister     // If i >= LastScreenRegister, goto WAIT
    D = M
    @i
    D = M-D
    @WAIT
    D; JGE

    @R0
    D = M
    @i
    A = M
    M = D

    @i                      // i++
    M = M+1
    @LOOP
    0; JMP

(END)
    @END
    0; JMP
