// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

@R2         // Reset output
M=0

@i          // Iterator var
M=1

(LOOP)
    @R1         // if i > R2 goto END
    D=M
    @i
    D=M-D
    @END
    D; JGT

    @i          // Increment i
    M=M+1

    @R0
    D=M
    @R2
    M=M+D

    @LOOP
    0; JMP

(END)
    @END
    0; JMP
